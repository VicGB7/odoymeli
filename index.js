const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./src/config/openapi.json');
const app = express();
const PORT = process.env.PORT || 3002

//LOGS
morgan.token('datetime', () => { 
    const date = new Date();
    const newDate = `${date.toLocaleDateString()} - ${date.toLocaleTimeString()}`
    return newDate
})
morgan.format('formatCS3', ':method,:url,:status,:res[content-length],[:datetime],[:remote-addr],:user-agent,:response-time ms');

//MIDDLEWARES
app.use(express.urlencoded({extended : true, limit: '50mb'}));
app.use(express.json({limit: '50mb'}));
app.use(morgan('formatCS3'));
app.use(cors({ origin: '*', methods: ['GET','POST','DELETE','PUT'] }));

//DOCUMENTATION
app.use('/api/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

//ROUTES
app.use('/home', (req, res) => { res.send({ message: 'Bienvenido...' })} )
app.use('/odoo', require('./src/routes/odooRoutes.js'))
app.use('/meli', require('./src/routes/meliRoutes.js'))
app.use('/ebay', require('./src/routes/ebayRoutes.js'))
app.use('/example', require('./src/routes/exampleRoutes.js'));
app.use((req, res) => {
    return res.status(404).json({ message: 'Not Found' });
})

//INIT SERVER
app.listen(PORT, (err) => { 
    console.log(err || `El servidor esta en el puerto -> ${PORT}`)
});