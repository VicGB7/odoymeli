const examples = require('./exampleDatabase.json')

const getExamples = async () => {
    return examples
}

module.exports = {
    getExamples
}