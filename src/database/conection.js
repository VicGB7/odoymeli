const { Pool } = require('pg');
const env = require('../config/config');

const pool = new Pool({
    host : env.DATABASE_IP,
    user : 'postgres',
    password : 'mfmssmcl',
    database: 'api-cs3',
    port: env.DATABASE_PORT
});

module.exports = pool