const exampleCRUD = require('../database/exampleCRUD')

const getAllExamples = async () => {
    const examples = await exampleCRUD.getExamples()
    if(Array.isArray(examples))
        return examples
    return [examples]
}

module.exports = {
    getAllExamples
}