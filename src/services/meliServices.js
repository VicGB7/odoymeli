const meli = require('../classes/meli')
const odoo = require('../classes/odoo')
const csv = require('csv-parser')
const fs = require('fs')

const getTokens = async () => {
    const tokens = await meli.getTokenAnyTime()
    return tokens
}

const getProducts = async () => {
    return { message: `Metodo en proceso` }
}

const getCategorys = async () => {
    const categorys = await meli.categorys()
    return categorys
}

const getSubcategories = async (category) => {
    const subcategories = await meli.subcategories(category)
    return subcategories
}

const predictCategory = async (title = ``) => {
    const category = await meli.predictCategory(`MLM`, title)
    return category[0]
}

const getAttributes = async (category = `MLM1747`) => {
    const attributes = await meli.attributes(category)
    return attributes
}

const getSpecificAttributes = async (domain) => {
    const specificAttributes = await meli.specificAttributes(domain)
    return specificAttributes
}

const determinatedAttribute = async (body) => {
    const { name_category, attribute, know_attributes } = body
    if(!name_category)
        return { badRequest: `El parametro 'name_category' no tiene datos validos` }
    if(!attribute)
        return { badRequest: `El parametro 'attribute' no tiene datos validos` }
    const determinateAttribute = meli.determinatedAttribute(name_category, attribute, know_attributes)
    return determinateAttribute
}

const getPublicationTypes = async () => {
    const types = await meli.getPublicationTypes()
    return types
}

const uploadProduct = async (body = {}) => {
    const uploadedProduct = await meli.upProduct(body)
    return uploadedProduct
}

const uploadProductAsync = async (ids = []) => {
    const schema = [
        'name','list_price','venta_meli','titulo','descripcion','categorias',
        'subcategorias1','subcategorias2','subcategorias3','marca','mpn','qty_available',
        'codigouniversaldeproducto','alturadelpaquete','anchodelpaquete','image_1920',
        'image_1920_2','image_1920_3','image_1920_4','image_1920_5',
        'largodelpaquete','pesodelpaquete','numeroderegistro','certificacioninmetro',
        'esinflamable','condiciondelitem','modelo','sku','eskit','tagsdescriptivos',
        'caracteristicasdelproducto','caracteristicasquimicasdelproducto','alimentosybebidas',
        'medicamentos','caracteristicasdelasbaterias','embalajedelenvio','informacionadicionalrequerida',
        'canalexclusivo','plataformasexcluidas','esaptoparaenvio','fuentedelproducto',
    ]
    for(const id of ids){
        const productOdoo = await odoo.read('product.template', [[[['id', '=', id]], schema]])
        if(productOdoo[0]['venta_meli']){
            const title = productOdoo[0]['titulo']
            const category = productOdoo[0]['subcategorias3'] ||  productOdoo[0]['subcategorias2'] || productOdoo[0]['subcategorias1'] || productOdoo[0]['categorias'] 
            const predictedCategory = await meli.predictCategory('MLM', category[1])
            const category_id = predictedCategory[0]['category_id']
            const price = Number(productOdoo[0]['list_price'])
            const available_quantity = Number(productOdoo[0]['qty_available'])
            const description = productOdoo[0]['descripcion']
            const sale_terms = []
            const images = await processImages([
                productOdoo[0]['image_1920'], productOdoo[0]['image_1920_2'], productOdoo[0]['image_1920_3'], productOdoo[0]['image_1920_4'], productOdoo[0]['image_1920_5']
            ])
            const values_id_attribute = await processAttributes(productOdoo[0], predictedCategory[0]['domain_id'])
            const attributes = [ ] //PENDIENTE
            attributes.push({ 'id': 'BRAND', 'name': 'Marca', 'value_id': values_id_attribute[0], 'value_name': productOdoo[0]['marca'] })
            attributes.push({ 'id': 'ITEM_CONDITION', 'value_name': 'Nuevo' })
            const product = { title, category_id, price, available_quantity, description, sale_terms, images, attributes }
            const publicated = await meli.upProduct(product)
            console.log(publicated['status'], publicated.cause, publicated.error)
            //Enviar notificacion a odoo
        }
        else
            console.log(`El producto con ID ${id} aun no esta configurado para venta de Mercado Libre`) //SEND NOTIFICATION
    }
}

const uploadProductCsv = async (path) => {
    try {
        const rows = await new Promise((resolve, reject) => {
            const list = []
            fs.createReadStream(path).pipe(csv())
                .on('data', (row) => { list.push(row); })
                .on('error', (error) => { console.log(error); reject(error) })
                .on('end', () => { fs.unlinkSync(path); resolve(list); });
        })
        if(!Array.isArray(rows))
            return console.log({ error: `No se logro conseguir los datos, verificar...`}) 
        for(const row of rows){
            const title =  `${row['DESCRIPCION']} para ${row['Codigo.Marca']}`
            const predictedCategory = await meli.predictCategory(`MLM`, title)
            if(predictedCategory[0]){
                const description = `${title}\nNumero de parte: ${row['NUMERO DE PARTE']}\n
                    Segmento: ${row['Segmento']}\nCartera: ${row['Cartera']}\nCompatibilidad: ...`;
                const available_quantity = Number(row['EXISTENCIA'])
                const category_id = predictedCategory[0]['category_id']
                const price = Number(row['VALUADO'].replace(/,/g, ''))
                const images = [Buffer(fs.readFileSync('./src/images/no_ofertar.gif')).toString('base64')]
                const attributes = []
                if(!row['attributes']){
                    attributes.push({ 'id': 'BRAND', 'name': 'Marca', 'value_id': '276243' ,'value_name': 'Genérica' })
                    attributes.push({ 'id': 'ITEM_CONDITION', 'value_name': 'Nuevo' })
                }
                else
                    attributes = predictedCategory[0]['attributes']
                const product = { title: `${title} - No Ofertar`, category_id, price, available_quantity, description, sale_terms: [], images, attributes }
                const publicated = await meli.upProduct(product)
                console.log((publicated['status']=='active')?`El producto se publico correctamente`:`No se logro subir el producto`)
            }
            else
                console.log(`El producto no esta bien definido -> ${row['DESCRIPTION']}, con posible categoria de 'Otros - MLM158087'`)
        }
        console.log(`uploadProductCsv: Todo se realizo de manera correcta, no hay errores`)
    }
    catch(error) {
        console.log(`uploadProductCsv -> `, error)
    }
}

async function processImages(images = []){
    const pictures = []
    for(const image of images)
        if(image)
            pictures.push(image)
    return pictures
}

async function getIdAttribute(attribute = `BRAND`, nameCategory = ``, valueIndex = `Genérica`){
    const attributes = await meli.determinatedAttribute(nameCategory, attribute)
    let id = ``
    if(attributes && Array.isArray(attributes) && valueIndex){
        for(const attribute of attributes){
            if(attribute.name == valueIndex){
                id = attribute.id
                break;
            }
        }   
    }
    return id
}

async function processAttributes(body, domain){
    //await getIdAttribute('BRAND', predictedCategory[0]['domain_id'], 'Genérica')
    const { marca, mpn, codigouniversaldeproducto,alturadelpaquete,anchodelpaquete,largodelpaquete,
    pesodelpaquete, numeroderegistro, certificacioninmetro, esinflamable, condiciondelitem, modelo,
    sku, eskit, tagsdescriptivos, caracteristicasdelproducto, caracteristicasquimicasdelproducto,
    alimentosybebidas, medicamentos, caracteristicasdelasbaterias, embalajedelenvio,
    informacionadicionalrequerida, canalexclusivo, plataformasexcluidas, esaptoparaenvio,
    fuentedelproducto } = body
    const id = []
    if(marca)
        id.push(await getIdAttribute('BRAND', domain, marca))
    return id
}

module.exports = {
    getTokens,
    getProducts,
    getCategorys,
    getSubcategories,
    predictCategory,
    getAttributes,
    getSpecificAttributes,
    determinatedAttribute,
    getPublicationTypes,
    uploadProduct,
    uploadProductAsync,
    uploadProductCsv,
}