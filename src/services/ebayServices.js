const Ebay = require('../classes/ebay')
const ebay = new Ebay()

const getTitles = async (article) => {
    const html = await ebay.search(article)
    const titles = await ebay.getTitles(html, 5)
    return titles
}         

const getInfoProduct = async (link) => {
    const html = await ebay.search(link)
    const information = ebay.getProduct(html)
    return information
}

module.exports = {
    getTitles, 
    getInfoProduct
}