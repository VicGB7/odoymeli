const odoo = require('../classes/odoo')
const odooSchemas = require('../schemas/odooSchema')

const getProducts = async () => {
    const products = await odoo.read('product.template', [[[], ['name']]])
    return products
}

const uploadProduct = async (title, images = [], compability = "", description = "") => {
    const param = await odooSchemas.schemaCreate(title, images, 
        `COMPATIBILIDAD\n${compability}\n\n\nDESCRIPCION\n${description}\n\n\n`)
    if(param.error)
        return param
    const result = await odoo.create('product.template', [[param]])
    if(result?.error)
        return { error: result.error }
    return result
}

module.exports = {
    getProducts,
    uploadProduct
}