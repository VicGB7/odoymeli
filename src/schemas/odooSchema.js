const fetch = require('node-fetch');

const schemaCreate = async (title, images = [], description = ``) => {
    if(!title)
        return { error: `atributo 'title' no tiene datos` }
    if(!Array.isArray(images) || images.length <= 0)
        return { error: `atributo 'images' no es un arreglo o no tiene datos` }
    return {
        name: title,
        display_name: title,
        titulo: title,
        venta_meli: true,
        es_prueba: false,
        descripcion: description || false,
        image_1920: await urlToImgBase64(images[0]) || false,
        imagen_2: await urlToImgBase64(images[1]) || false,
        imagen_3: await urlToImgBase64(images[2]) || false,
        imagen_4: await urlToImgBase64(images[3]) || false,
        imagen_5: await urlToImgBase64(images[4]) || false,
    }
}

const urlToImgBase64 = async (url) => {
    try {
        if(!url)
            return undefined
        const fetched = await fetch(url)
        const buffer = await fetched.buffer()
        return buffer.toString('base64')   
    } 
    catch (error) {
        console.log(error)
        return undefined
    }
}

module.exports = {
    schemaCreate
}