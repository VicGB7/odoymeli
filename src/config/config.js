const path = require('path');
const data = require('dotenv').config({
    path: path.resolve(__dirname, `../../environments/.env.${process.env.NODE_ENV}`)
});

module.exports = { 
    DATABASE_IP: data.parsed.DATABASE_IP,
    DATABASE_PORT: data.parsed.DATABASE_PORT,
    APP_ID_MELI: data.parsed.APP_ID_MELI,
    CLIENT_SECRET_MELI: data.parsed.CLIENT_SECRET_MELI,
    REFRESH_TOKEN_MELI:  data.parsed.REFRESH_TOKEN_MELI,
    USER_ODOO: data.parsed.ODOO_USER,
    PASS_ODOO: data.parsed.ODOO_PASS,
    IP_ODOO: data.parsed.ODOO_IP,
    PORT_ODOO: data.parsed.ODOO_PORT,
    DB_ODOO: data.parsed.ODOO_DB
}