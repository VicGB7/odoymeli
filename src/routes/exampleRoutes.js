const { Router } = require('express');
const router = Router();
const exampleController = require('../controllers/exampleController')

router
    .get('/getExamples', exampleController.getExamples)
    .get('/index', (req, res) => { res.send('Prueba dos') })

module.exports = router