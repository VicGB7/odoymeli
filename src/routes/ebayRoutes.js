const { Router } = require('express')
const router = Router()
const ebayController = require('../controllers/ebayController')

router
    .get('/titles/:article', ebayController.getTitles)
    .post('/product/info', ebayController.getInfoProduct);

module.exports = router