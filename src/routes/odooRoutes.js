const { Router } = require('express')
const router = Router()
const odooController = require('../controllers/odooController')

router
    .get('/products', odooController.getProducts)
    .post('/products', odooController.uploadProduct)

module.exports = router