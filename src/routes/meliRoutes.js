const { Router } = require('express')
const router = Router()
const meliController = require('../controllers/meliController')
const multer = require('multer')
const uploadFile = multer({ dest: './src/tmp/' })
const { body, validationResult, check } = require('express-validator')

router
    .get('/token', meliController.getTokens)
    .get('/products', meliController.getProducts)
    .get('/attributes', meliController.getAttributes)
    .get('/attributes/specific/:domain', meliController.getSpecificAttributes)
    .get('/categories', meliController.getCategorys)
    .get('/subcategories/:category', meliController.getSubcategories)
    .get('/category/predict', meliController.predictCategory)
    .get('/publication/types', meliController.getPublicationTypes)
    .post('/upload/product', meliController.uploadProduct)
    .post('/upload/product/async', meliController.uploadProductAsync)
    .post('/upload/product/csv', uploadFile.single('file'), meliController.uploadProductCsv)
    .post('/attribute/determinated', meliController.determinatedAttribute)

module.exports = router