const Odoo = require('odoo-xmlrpc')
const env = require('../config/config')

const odoo = new Odoo({
    url: `http://${env.IP_ODOO}`,
    port: env.PORT_ODOO,
    db: env.DB_ODOO,
    username: env.USER_ODOO,
    password: env.PASS_ODOO
});

const create = (model = ``, params = [[{ 'var': 'value' }]]) => {
    return new Promise(function(resolve, reject){
        odoo.connect(function (error) {
            if (error) { console.log(`erroror en 'create-connect' -> ${error}`); return reject({ error }); }
            odoo.execute_kw(model, 'create', params, function (error, value) {
                if (error) { console.log(`erroror en 'create-execute_kw' -> ${error}`); return reject({ error }); }
                resolve({ id : value })
            });
        });
    })
}

const read = (model = ``, params = [[[]]] || [[[['con','dit','ion'], ['attributes']]]]) => {
    return new Promise(function(resolve, reject){
        odoo.connect(function (error) {
            if (error) { console.log(`erroror en 'connect - read' -> `, error); return reject({ error }); }
            odoo.execute_kw(model, 'search_read', params, function (error, value) {
                if (error) { console.log(`erroror en 'execute_kw - read' -> `, error); return reject({ error }); }
                resolve(value);
            });
        });
    })
}

const update = (model = ``, params = [[['id_to_update'], { 'varToUpdate': 'value' }]]) => {
    return new Promise(function(){
        odoo.connect(function (error) {
            if (error) { console.log(`erroror en 'connect-update' -> `, error); return reject({ error }); }
            odoo.execute_kw(model, 'write', params, function (error, value) {
                if (error) { console.log(`erroror en 'execute_kw-update' -> `, error); return reject({ error }); }
                resolve(value)
            });
        });
    })
}

/**
 * Delete an object in odoo using only its id
 * @param  {String} model Name model from odoo
 * @param  {Array} params Array with content other array with an id inside
 */
const remove = async (model, params = [['id']]) => {
    return new Promise((resolve, reject) => {
        odoo.connect(function (error) {
            if (error) { console.log(`erroror en 'connect-remove' -> `, error); return reject({ error });  }
            odoo.execute_kw(model, 'unlink', params, function (error, value) {
                if (error) { console.log(`erroror en 'execute_kw-remove' -> `, error); return reject({ error }); }
                resolve(value)
            });
        });  
    })
}

module.exports = {
    create,
    read,
    update,
    remove
}