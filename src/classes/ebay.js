const cheerio = require('cheerio')
const fetch = require('node-fetch')

class Ebay{

    constructor(){
        this.error = undefined
    }

    async search (article = ``) {
        try {
            const url = (article.includes('https://www.ebay.com'))?article:`https://www.ebay.com/sch/i.html?_from=R40&_nkw=${article}&_sacat=0&_pgn=1`;
            const fetched = await fetch(url)
            const text = await fetched.text()
            const buffer = Buffer.from(text)
            return buffer.toString()
        } 
        catch (error) {
            this.error = error
            return ``
        }
    }

    getTitles = async (html = ``, quantity = 5) => {
        const titles = []
        try {
            const $ = cheerio.load(`${html}`)
            $('.s-item.s-item__pl-on-bottom').each((position, element) => {
                if(position == 0 || position > quantity) 
                    return ;
                const title = $(element).find('.s-item__title').text()
                const link = $(element).find('.s-item__link').attr('href')
                titles.push({ position, title, link })
            })
        } 
        catch (error) {
            console.log(error)
            titles.push({ position: -1, title: error, link: `` })
        }
        finally{
            return titles
        }
    }

    getImages = (html = ``) => {
        const images = []
        try {
            const $ = cheerio.load(`${html}`)
            const exist = $('.ux-image-filmstrip-carousel').length
            if(exist){
                $('.ux-image-filmstrip-carousel').find('button.ux-image-filmstrip-carousel-item').each((image, el) => {
                    const img = $(el).find('img')
                    const src = $(img).attr('src')
                    images.push({ image: image+1, src })
                })
            }
            else{
                const img = $('.ux-image-carousel-container')
                const src = $(img).find('img').attr('src')
                images.push({ image: 1, src })
            }
        } 
        catch (error) {
            console.log(error)
            images.push({ image: -1, src: error })
        }
        finally{
            return images
        }
    }

    getCompability = (html = ``) => {
        try {
            const $ = cheerio.load(`${html}`)
            const content = $('.tab-content-m ')
            const section = $(content).find('.motors-compatibility-table-wrapper')
            const table = $(section).find('table').text()
            return table || ``
        } 
        catch (error) {
            console.log(error)
            return error
        }
        
    }

    getCharacteristics = (html = ``) => {
        try {
            const $ = cheerio.load(`${html}`)
            const content = $('#vi-desc-maincntr').find('.vim.x-about-this-item-evo')
            const table = $(content).find('.ux-layout-section-evo').text()
            return table || ``
        } 
        catch (error) {
            console.log(error)
            return error
        }
    }

    getProduct = (html = ``) => {
        try {
            const $ = cheerio.load(`${html}`)
            const panelCentral = $('#CenterPanelInternal')
            const title = $(panelCentral).find('.x-item-title__mainTitle').text()
            const sectionImages = $(panelCentral).find('#vi_main_img_fs').html() || $(panelCentral).find('#PicturePanel').html()
            const images = this.getImages(`${sectionImages}`)

            const about = $('div.tabbable').html()
            const compability = this.getCompability(about)
            const characteristics = this.getCharacteristics(about)

            return { title, images, compability, characteristics }
        } 
        catch (error) {
            console.log(error)
            return { title: `Error`, images: [], compability: ``, characteristics: error }
        }
    }

    getError = _ => {
        return this.error
    }

}

module.exports = Ebay