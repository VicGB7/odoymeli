const axios = require('axios');
const env = require('../config/config')
const fs = require('fs')
const FormData = require('form-data')
const sleep = (seconds = 1) => { return new Promise((resolve) => {setTimeout(resolve, seconds * 1000); });}
let tokenMeli = ``, refresh = true

const getTokenAnyTime = async () => {
    if(!refresh)
        return { access_token: tokenMeli }
    try{
        const data = {
            grant_type: 'refresh_token',
            client_id: env.APP_ID_MELI,
            client_secret: env.CLIENT_SECRET_MELI,
            refresh_token: env.REFRESH_TOKEN_MELI 
        }
        const config = postConfig(`https://api.mercadolibre.com/oauth/token`, data)
        const response = await axios(config)
        if(response.data?.access_token){
            timer(response.data.expires_in)
            tokenMeli = response.data.access_token
        }
        console.log(tokenMeli)
        return response.data
    }
    catch(error){
        console.log(`Error 'getTokenAnyTime' -> `, error.response?.data)
        return error.response?.data
    } 
}

const categorys = async () => {
    try{
        if(refresh)
            await getTokenAnyTime()
        const options = getConfig('https://api.mercadolibre.com/sites/MLM/categories')
        const response = await axios(options)
        return response.data
    }
    catch(error){
        console.log(`Error 'categorys' -> `, error.response?.data)
        return error.response?.data
    } 
}

const subcategories = async (category = ``) => {
    try{
        const config = getConfig(`https://api.mercadolibre.com/categories/${category}`)
        const response = await axios(config)
        return response.data
    }
    catch(error){
        console.log(`Error 'subcategories' -> `, error.response?.data)
        return error.response?.data
    }
}

const attributes = async (category = ``) => {
    try{
        if(refresh)
            await getTokenAnyTime()
        const options = getConfig(`https://api.mercadolibre.com/categories/${category}/attributes`)
        const response = await axios(options)
    return response.data
    }
    catch(error){
        console.log(`Error 'attributes' -> `, error.response?.data)
        return error.response?.data
    }
}

const getPublicationTypes = async () => {
    try{
        if(refresh)
            await getTokenAnyTime()
        const config = getConfig(`https://api.mercadolibre.com/sites/MLM/listing_types`)
        const response = await axios(config)
        return response.data
    }
    catch(error){
        console.log(`Error 'getPublicationTypes' -> `, error.response?.data)
        return error.response?.data
    }
}

const predictCategory = async (zone = `MLM`, title = ``) => {
    try{
        if(refresh)
            await getTokenAnyTime()
        const config = getConfig(`https://api.mercadolibre.com/sites/${zone}/domain_discovery/search?limit=1&q=${title}`)
        const response = await axios(config)
        return response.data
    }
    catch(error){
        console.log(`Error 'predictedCategory' -> `, error.response?.data)
        return error.response?.data
    }
}

const upProduct = async (body = {}) => {
    if(refresh)
        await getTokenAnyTime()
    const { title, category_id, price, available_quantity, description, sale_terms = [], images = [], attributes = [] } = body
    if(!images.length)
        return { message: 'No se recibio alguna imagen' }
    const product = {
        title, category_id, price, available_quantity, attributes, sale_terms, description: {text_plain: description},
        currency_id: 'MXN', buying_mode: 'buy_it_now', condition: 'new', listing_type_id: 'gold_special', pictures: []
    }
    for(const image of images){
        fs.writeFileSync('./src/tmp/meli.jpg', image, { encoding:'base64' })
        const source = await imgToUrlMeli(`meli.jpg`)
        product.pictures.push({ source })
        fs.unlinkSync('./src/tmp/meli.jpg')
    }
    try{
        const response = await axios.post(`https://api.mercadolibre.com/items`, product, { headers: {
            'Authorization': `Bearer ${tokenMeli}`,
            'Content-Type': 'application/json'
        }})
        return response.data
    }
    catch(error){
        console.log(`Error 'UpProduct' -> `, error.response?.data)
        return error.response?.data
    }     
}

const specificAttributes = async (domain = ``) => {
    try{
        const config = getConfig(`https://api.mercadolibre.com/catalog_domains/${domain}`)
        const response = await axios(config)
        return response.data
    }
    catch(error){
        console.log(`Error 'specificAttributes' -> `, error.response?.data)
        return error.response?.data
    }
    //MLM-VEHICLE_ENGINE_PISTON_RING_SETS
}

const determinatedAttribute = async (nameCategory = ``, attribute = `BRAND`, know_attributes = []) => {
    //MLA-CARS_AND_VANS - BRAND
    if(!nameCategory || !attribute)
        return []
    try{
        const response = await axios.post(`https://api.mercadolibre.com/catalog_domains/${nameCategory}/attributes/${attribute}/top_values`, { know_attributes },
        { 
            headers: {
                Authorization: `Bearer ${tokenMeli}`,
                'Content-Type': 'application/json'    
            }
        });
        return response.data
    }
    catch(error){
        console.log(`Error 'determinatedAttribute' -> `, error.response?.data)
        return error.response?.data
    }
}

const imgToUrlMeli = async (filename) => {   
    if(refresh)
        await getTokenAnyTime()
    try{
        const formData = new FormData();
        formData.append('file', fs.createReadStream(`${process.cwd()}/src/tmp/${filename}`));
        const response = await axios.post(`https://api.mercadolibre.com/pictures/items/upload`, formData, { headers: {
            'Authorization': `Bearer ${tokenMeli}`,
            ...formData.getHeaders()    
        }});
        const urls = response.data.variations
        return urls[0]['url']
    } 
    catch(error){
        console.log(`Error 'imgToUrlMeli' -> `, error.response?.data)
        return error.response?.data
    }
}

function getConfig(url = ``){
    return {
        method: 'GET',
        maxBodyLength: Infinity,
        url,
        headers: { 'Authorization': `Bearer ${tokenMeli}` }
    };
}

function postConfig(url = ``, data){
    return {
        method: 'POST',
        maxBodyLength: Infinity,
        url,
        headers: { 
            'accept': 'application/json', 
            'content-type': 'application/x-www-form-urlencoded',
            'Authorization': `Bearer ${tokenMeli}`
        },
        data: data
    }
}

async function timer(seconds){
    refresh = false
    await sleep(seconds)
    refresh = true
}

module.exports = {
    getTokenAnyTime,
    categorys,
    subcategories,
    predictCategory,
    attributes,
    specificAttributes,
    determinatedAttribute,
    upProduct,
    imgToUrlMeli,
    getPublicationTypes,
}