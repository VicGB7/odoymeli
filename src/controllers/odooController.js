const odooServices = require('../services/odooServices')

const getProducts = async (req, res) => {
    const gotProducts = await odooServices.getProducts()
    return res.send({ gotProducts })
}

const uploadProduct = async (req, res) => {
    const { title, images, compability, description } = req.body
    const uploaded = await odooServices.uploadProduct(title, images, compability, description)
    return res.status(201).send(uploaded)
}

module.exports = {
    getProducts,
    uploadProduct
}