const meliServices = require('../services/meliServices')

const getTokens = async (req, res) => {
    const gotTokens = await meliServices.getTokens()
    if(gotTokens.error)
        return res.status(404).send({ error: gotTokens })
    return res.send({ tokens: gotTokens })
}

const getProducts = async (req, res) => {
    const gotProducts = await meliServices.getProducts()
    return okResponse(res, { gotProducts })
}

const getCategorys = async (req, res) => {
    const gotCategorys = await meliServices.getCategorys()
    return okResponse(res, { categories: gotCategorys})
}

const getSubcategories = async (req, res) => {
    const { category } = req.params
    const gotSubcategories = await meliServices.getSubcategories(category)
    return okResponse(res, gotSubcategories)
}

const predictCategory = async (req, res) => {
    const { title } = req.query
    const predictedCategory = await meliServices.predictCategory(title)
    return res.send(predictedCategory)
}

const getAttributes = async (req, res) => {
    const { category } = req.query.category
    const gotAttributes = await meliServices.getAttributes(category)
    return okResponse(res, { attributes: gotAttributes })
}

const getSpecificAttributes = async (req, res) => {
    const { domain } = req.params
    const gotAttributes = await meliServices.getSpecificAttributes(domain)
    return okResponse(res, gotAttributes)
}

const determinatedAttribute = async (req, res) => {
    const determinatedAttribute = await meliServices.determinatedAttribute(req.body)
    if(determinatedAttribute.badRequest)
        return badRequest(res, { determinatedAttribute })
    if(determinatedAttribute.error)
        return errorResponse(res, { determinatedAttribute })
    return okResponse(res, { determinatedAttribute })
}

const getPublicationTypes = async (req, res) => {
    const gotTypes = await meliServices.getPublicationTypes()
    return okResponse(res, { publicationsTypes: gotTypes })
}

const uploadProduct = async (req, res) => {
    const { body } = req
    const uploadedProduct = await meliServices.uploadProduct(body)
    console.log(body)
    console.log(uploadedProduct)
    if(uploadedProduct.error)
        return badRequest(res, uploadedProduct)
    return createResponse(res, uploadedProduct)
    
}

const uploadProductAsync = async (req, res) => {
    const { ids } = req.body;
    console.log(ids)
    if(!ids || !Array.isArray(ids) || ids.some((id) => ((id == "") || isNaN(id))))
        return badRequest(res, { error: `El parametro 'ids' no tiene datos validos, verificar` })
    meliServices.uploadProductAsync(ids)
    return acceptedResponse(res, { data: `Los id's -> [${ids}] se recibieron correctamente, procesando datos de manera asyncrona...` })
}

const uploadProductCsv = async (req, res) => {
    const { path } = req.file
    if(!path)
        return errorResponse(res, { error: `El archivo no se logro subir correctamente` })
    meliServices.uploadProductCsv(path) //Procesando asyncronamente los productos
    acceptedResponse(res, { 
        data: `El archivo en ${(path).replace(/\\/g, '/')} se recibio correctamente, procesando datos de manera asyncrona...` 
    })
}

function okResponse(res, msg = ``){
    return res.send(msg)
}

function createResponse(res, msg = ``){
    return res.status(201).send(msg)
}

function acceptedResponse(res, msg = ``){
    return res.status(202).send(msg)
}

function errorResponse(res, msg = ``){
    return res.status(404).send(msg)
}

function badRequest(res, msg = ``){
    return res.status(400).send(msg)
}

module.exports = {
    getTokens,
    getProducts,
    getAttributes,
    getSpecificAttributes,
    determinatedAttribute,
    getCategorys,
    getSubcategories,
    predictCategory,
    getPublicationTypes,
    uploadProduct,
    uploadProductAsync,
    uploadProductCsv,
}