const exampleServices = require('../services/exampleServices')

const getExamples = async (req, res, next) => {
    const examples =  await exampleServices.getAllExamples()
    res.send({ status: 'OK', examples})
}

module.exports = {
    getExamples
}