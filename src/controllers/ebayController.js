const ebayServices = require('../services/ebayServices')
const { isValidUrl } = require('../utils/validations') 

const getTitles = async (req, res) => {
    const { article } = req.params
    const titles = await ebayServices.getTitles(article)
    res.send(titles)
}

const getInfoProduct = async (req, res) => {
    const { link } = req.body;
    if(!link || !isValidUrl(link))
        return res.status(400).send({ message: `link no cuenta con datos validos` })
    const information = await ebayServices.getInfoProduct(link)
    res.send(information)
}

module.exports = {
    getTitles,
    getInfoProduct
}